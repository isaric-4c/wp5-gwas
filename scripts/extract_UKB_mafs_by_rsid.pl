#!/usr/bin/perl -w

use IO::File;
use strict;

my $rsid_input = shift;

if (!$rsid_input)
{
    die "Usage: cat UKB_MAFs/* | perl extract_UKB_mafs_by_rsid.pl <rsids.txt>\n";
}

my $in_fh = new IO::File;
$in_fh->open($rsid_input, 'r') or die "Could not open $rsid_input\n$!";

my %rsids;
while (my $line = <$in_fh>)
{
    chomp $line;
    $rsids{$line}++;
}

$in_fh->close();

while (my $line = <>)
{
    my @tokens = split(/\t/, $line);
    if (exists($rsids{$tokens[1]}))
    {
	print $line;
    }
}
