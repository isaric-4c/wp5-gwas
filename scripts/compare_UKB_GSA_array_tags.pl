#!/usr/bin/perl -w

use IO::File;
use strict;

my $ukb_input = shift;
my $gsa_input = shift;
my $pos_or_alleles = shift;

if (!$ukb_input || !$gsa_input || !$pos_or_alleles)
{
    die "Usage: perl compare_UKB_GSA_array_tags.pl <ukb_csv> <gsa_csv> <pos_only|alleles>\n";
}

my %tags;

my $in_fh = new IO::File;
$in_fh->open($ukb_input, 'r') or die "Could not open $ukb_input\n$!";

while (my $line = <$in_fh>)
{
    # skip header lines;
    next if ($line =~ /^#/);
    next if ($line =~ /Probe Set ID/);

    # clean up the input line
    chomp $line;
    $line =~ s/\"//g;

    # extract rsid, locus, and alleles
    my @tokens = split(/,/, $line);
    my $rsid = $tokens[2];
    my $chr = $tokens[4];
    my $pos = $tokens[5];
    my $ref = $tokens[13];
    my $alt = $tokens[14];

    next if ($pos =~ /-/);

    if ($pos_or_alleles eq "alleles")
    {
	push(@{ $tags{$chr}{$pos}{$ref}{$alt}{'rsid'} }, $rsid);
	$tags{$chr}{$pos}{$ref}{$alt}{'ukb'} = 1;
    }
    elsif ($pos_or_alleles eq "pos_only")
    {
	push(@{ $tags{$chr}{$pos}{'rsid'} }, $rsid);
	$tags{$chr}{$pos}{'ukb'} = 1;
    }
}

$in_fh->close();

$in_fh->open($gsa_input, 'r') or die "Could not open $gsa_input\n$!";

while (my $line = <$in_fh>)
{
    # skip header lines;
    next if ($line =~ /^\[/);
    next if ($line =~ /(Descriptor|Illumina|Assay|Date|Count|Name|Controls|Staining|Removal|Hybridization|Stringency|Polymorphic|Binding|Restoration)/);

    # clean up the input line
    chomp $line;

    # extract locus, and alleles
    my @tokens = split(/,/, $line);
    my $chr = $tokens[9];
    my $pos = $tokens[10];
   
    next if (!$pos);
    next if ($pos =~ /-/);

    my $snp = $tokens[3];
    my $ilmn_strand = $tokens[2];
    my $src_strand = $tokens[15];

    $snp =~ s/[\[\]]//g;
    my ($a, $b) = split('/', $snp);

    if ($ilmn_strand ne $src_strand)
    {
	$a =~ tr/ACGT/TGCA/;
	$b =~ tr/ACGT/TGCA/;
    }

    if ($pos_or_alleles eq "alleles")
    {
	if (exists($tags{$chr}{$pos}{$a}{$b}))
	{
	    $tags{$chr}{$pos}{$a}{$b}{'gsa'} = 1;
	}
	elsif (exists($tags{$chr}{$pos}{$b}{$a}))
	{
	    $tags{$chr}{$pos}{$b}{$a}{'gsa'} = 1;
	}
	else
	{
	    $tags{$chr}{$pos}{$a}{$b}{'gsa'} = 1;
	}
    }
    elsif ($pos_or_alleles eq "pos_only")
    {
	$tags{$chr}{$pos}{'gsa'} = 1;
    }
}

$in_fh->close();

foreach my $chr (sort keys %tags)
{
    foreach my $pos (sort { $a <=> $b } keys %{ $tags{$chr} })
    {
	if ($pos_or_alleles eq "pos_only")
	{
	    if (exists($tags{$chr}{$pos}{'ukb'}))
	    {
		if (exists($tags{$chr}{$pos}{'gsa'}))
		{
		    printf "$chr\t$pos\t%s\tboth\n", join(",", @{ $tags{$chr}{$pos}{'rsid'} });
		}
		else
		{
		    printf "$chr\t$pos\t%s\tukb_only\n", join(",", @{ $tags{$chr}{$pos}{'rsid'} });
		}
	    }
	    elsif (exists($tags{$chr}{$pos}{'gsa'}))
	    {
		print "$chr\t$pos\t.\tgsa_only\n";
	    }
	}
	elsif ($pos_or_alleles eq "alleles")
	{
	    foreach my $ref (sort keys %{ $tags{$chr}{$pos} })
	    {
		foreach my $alt (sort keys %{ $tags{$chr}{$pos}{$ref} } )
		{
		    if (exists($tags{$chr}{$pos}{$ref}{$alt}{'ukb'}))
		    {
			printf "$chr\t$pos\t$ref\t$alt\t%s\t", join(",", @{ $tags{$chr}{$pos}{$ref}{$alt}{'rsid'} });
			if (exists($tags{$chr}{$pos}{$ref}{$alt}{'gsa'}))
			{
			    print "both\n";
			}
			else
			{
			    print "ukb_only\n";
			}
		    }
		    elsif (exists($tags{$chr}{$pos}{$ref}{$alt}{'gsa'}))
		    {
			print "$chr\t$pos\t$ref\t$alt\t.\tgsa_only\n";
		    }
		}
	    }
	}
    }
}
