#!/bin/bash
#
# Merges a set of VCF files.
#
# qsub -t 1-X submit_merge_batches.sh <CONFIG_SH> <WORK_DIR> <BATCH_SIZE> <LENGTH>
#
# CONFIG_SH - absolute path to configuration script setting environment variables
#    * PARAMS_DIR
#    * PATH (to include bcftools and tabix)
# WORK_DIR - absolute path to output directory
#
#$ -N merge_batch
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -l h_rt=48:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG_SH=$1
WORK_DIR=$2
BATCH_SIZE=$3
LENGTH=$4

source $CONFIG_SH

cd $WORK_DIR

END_INDEX=$(($SGE_TASK_ID * $BATCH_SIZE))
if [[ $END_INDEX -lt $LENGTH ]]
then
  ls *.formatted.vcf.gz | head -n $END_INDEX | tail -n $BATCH_SIZE > input_args.$SGE_TASK_ID.txt
else
  TAIL_LENGTH=$(($END_INDEX - $BATCH_SIZE - $LENGTH))
  ls *.formatted.vcf.gz | tail -n $TAIL_LENGTH > input_args.$SGE_TASK_ID.txt
fi

bcftools merge -m both -l input_args.$SGE_TASK_ID.txt | bgzip -c > merged.${SGE_TASK_ID}.vcf.gz
tabix merged.${SGE_TASK_ID}.vcf.gz
