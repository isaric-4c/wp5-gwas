#!/bin/bash
#PBS -l walltime=48:00:00
#PBS -l ncpus=1,mem=8gb
#PBS -q sgp
#PBS -N tabix_gvcf
#PBS -j oe

# enable running singletons
if [ -z $PBS_ARRAY_INDEX ]
then
  if [ -z $INDEX ]
  then
    export PBS_ARRAY_INDX=1
  else
    export PBS_ARRAY_INDEX=$INDEX
  fi
fi

#
# Runs tabix indexing on a GVCF file.
#
# Expects environment variables to be set:
#
# CONFIG_SH - absolute path to configuration script setting environment variables
#    * PARAMS_DIR
#    * PATH (to include tabix)
# GVCF_FILES - path relative to PARAMS_DIR, list of GVCF files to process
#
# Tabix indexes will reside with the GVCF files.
#

source $CONFIG_SH

GVCF_FILE=`head -n $PBS_ARRAY_INDEX $PARAMS_DIR/$GVCF_FILES | tail -n 1`

if [ ! -e $GVCF_FILE.tbi ]
then
  tabix -p vcf $GVCF_FILE
fi

chmod 440 $GVCF_FILE.tbi

