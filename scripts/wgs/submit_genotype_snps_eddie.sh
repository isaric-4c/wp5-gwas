#!/bin/bash
#
# Genotypes a set of GVCF files against a given set of SNP positions and adds in the
# alternate allele where needed, stripping out the non-GT FORMAT fields as these do
# not work with bcftools merge.
#
# qsub -t 1-X submit_genotype_snps_eddie.sh <CONFIG_SH> <GVCF_FILES> <SNP_POSITIONS> <OUTPUT_DIR>
#
# CONFIG_SH - absolute path to configuration script setting environment variables
#    * PARAMS_DIR
#    * PATH (to include bcftools, tabix, and gatk)
#    * REFERENCE_GENOME
# GVCF_FILES - path relative to PARAMS_DIR, list of GVCF files to process
# SNP_POSITIONS	- path relative to PARAMS_DIR, set of SNPs in BED format (https://gatk.broadinstitute.org/hc/en-us/articles/360035531852-Intervals-and-interval-lists)
#               - *must* have set of the same positions in format chr:pos:ref:alt with suffix .txt instead of .bed
# OUTPUT_DIR - absolute path to output directory
#
#$ -N genotype_gvcf
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -l h_rt=48:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG_SH=$1
GVCF_FILES=$2
SNP_POSITIONS=$3
OUTPUT_DIR=$4

source $CONFIG_SH

echo PATH=$PATH
echo REFERENCE_GENOME=$REFERENCE_GENOME
echo GVCF_FILES=$PARAMS_DIR/$GVCF_FILES
echo SNP_POSITIONS=$PARAMS_DIR/$SNP_POSITIONS
echo OUTPUT_DIR=$OUTPUT_DIR

GVCF_FILE=`head -n $SGE_TASK_ID $PARAMS_DIR/$GVCF_FILES | tail -n 1`
BNAME=`basename $GVCF_FILE`
VCF_FILE=${BNAME%gvcf.gz}vcf.gz
LOG_FILE=${BNAME%gvcf.gz}genotype.log

cd $OUTPUT_DIR
echo VCF_FILE=$OUTPUT_DIR/$VCF_FILE
echo LOG_FILE=$OUTPUT_DIR/$LOG_FILE

gatk GenotypeGVCFs \
  -V $GVCF_FILE \
  -O ${VCF_FILE} \
  -R $REFERENCE_GENOME \
  --intervals $PARAMS_DIR/$SNP_POSITIONS \
  --force-output-intervals $PARAMS_DIR/$SNP_POSITIONS \
  &> $LOG_FILE

zcat $VCF_FILE | \
  perl $SCRIPTS_DIR/add_alt_allele.pl $PARAMS_DIR/${SNP_POSITIONS%.bed}.txt | \
  bcftools filter -e 'FORMAT/DP < 8' | \
  bgzip -c > ${VCF_FILE%.vcf.gz}.temp.vcf.gz
tabix ${VCF_FILE%.vcf.gz}.temp.vcf.gz

bcftools annotate -c ID --collapse some -a $PARAMS_DIR/${SNP_POSITIONS%.bed}.vcf.gz -x FORMAT ${VCF_FILE%.vcf.gz}.temp.vcf.gz | \
  bgzip -c  > ${VCF_FILE%.vcf.gz}.formatted.vcf.gz
tabix ${VCF_FILE%.vcf.gz}.formatted.vcf.gz

rm ${VCF_FILE%.vcf.gz}.temp.vcf.gz*
