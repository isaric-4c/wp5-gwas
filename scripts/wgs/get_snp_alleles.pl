#!/usr/bin/perl -w

use strict;
use warnings;
use Bio::EnsEMBL::Registry;

my $registry = 'Bio::EnsEMBL::Registry';

$registry->load_registry_from_db(
  -host => 'ensembldb.ensembl.org',
  -user => 'anonymous'
);

print "loaded registry\n";

# get the database adaptor for Slice objects
my $slice_adaptor = $registry->get_adaptor('homo_sapiens', 'core', 'slice');

print "fetched slice adaptor\n";

# get adaptor to VariationFeature object
my $vf_adaptor = $registry->get_adaptor('homo_sapiens', 'variation', 'VariationFeature');

print "fetched variation adaptor\n";

# get slice for location
my $slice = $slice_adaptor->fetch_by_region('chromosome', 1, 792461, 792461); 

print "fetched slice\n";

#return ALL variations defined in $slice
my $vfs = $vf_adaptor->fetch_all_by_Slice($slice); 

print "fetched variation features\n";

foreach my $vf (@{$vfs}){
  print "Variation: ", $vf->variation_name, " with alleles ", $vf->allele_string, 
        " in chromosome ", $slice->seq_region_name, " and position ", $vf->start, "-", $vf->end, "\n";
}
