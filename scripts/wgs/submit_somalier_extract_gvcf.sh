#!/bin/bash
#PBS -l walltime=1:00:00
#PBS -l ncpus=1,mem=2gb
#PBS -q sgp
#PBS -N somalier_extract
#PBS -j oe

# enable running singletons
if [ -z $PBS_ARRAY_INDEX ]
then
  if [ -z $INDEX ]
  then
    export PBS_ARRAY_INDX=1
  else
    export PBS_ARRAY_INDEX=$INDEX
  fi
fi

#
# Runs somalier extract on GEL GVCF files
#
# Expects environment variables to be set:
#
# CONFIG_SH - absolute path to configuration script setting environment variables
#    * PARAMS_DIR
#    * PATH (to include somalier)
#    * RESOURCES_DIR
#    * REFERENCE_GENOME
# GVCF_FILES - path relative to PARAMS_DIR, list of GVCF files to process
# SOMALIER_SITES - path relative to RESOURCES_DIR, somalier sites file downloaded from https://github.com/brentp/somalier/releases/tag/v0.2.11
# EXTRACT_DIR - path for somalier extract output
#

source $CONFIG_SH

echo PARAMS_DIR=$PARAMS_DIR
echo PATH=$PATH
echo REFERENCE_GENOME=$REFERENCE_GENOME
echo GVCF_FILES=$PARAMS_DIR/$GVCF_FILES
echo SOMALIER_SITES=$RESOURCES_DIR/$SOMALIER_SITES
echo EXTRACT_DIR=$EXTRACT_DIR

GVCF_FILE=`head -n $PBS_ARRAY_INDEX $PARAMS_DIR/$GVCF_FILES | tail -n 1`

cd $EXTRACT_DIR

somalier extract \
  -f $REFERENCE_GENOME \
  -s $RESOURCES_DIR/$SOMALIER_SITES \
  $GVCF_FILE

