#!/usr/bin/perl -w

#
# Adds a specified alternate allele given the chr, pos, and ref info
#

use IO::File;
use strict;

my $site_file = shift;
my $site_fh = new IO::File($site_file, "r") or die "Could not open $site_file\n$!";

my %sites;
while (my $line = <$site_fh>)
{
	chomp $line;
	my ($chr, $pos, $ref, $alt) = split(/:/, $line);
	$sites{"$chr:$pos:$ref"} = $alt;
}

$site_fh->close();

while (my $line = <>)
{
	chomp $line;
	if ($line =~ /^#/)
	{
		print "$line\n";
		next;
	}

	my @tokens = split(/\t/, $line);
	if ($tokens[4] eq "." && exists($sites{"$tokens[0]:$tokens[1]:$tokens[3]"}))
	{
		$tokens[4] = $sites{"$tokens[0]:$tokens[1]:$tokens[3]"};
	}

	printf "%s\n", join("\t", @tokens);
}

