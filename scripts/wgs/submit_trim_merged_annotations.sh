#!/bin/bash
#
# Merges a set of VCF files.
#
# qsub -t 1-X submit_trim_merged_annotations.sh <CONFIG_SH> <WORK_DIR>
#
# CONFIG_SH - absolute path to configuration script setting environment variables
#    * PARAMS_DIR
#    * PATH (to include bcftools and tabix)
# WORK_DIR - absolute path to output directory
#
#$ -N trim_merged_annotations
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=4G
#$ -l h_rt=1:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG_SH=$1
WORK_DIR=$2

source $CONFIG_SH

cd $WORK_DIR

bcftools annotate -x ^INFO/DP,INFO/AN,INFO/AC merged.$SGE_TASK_ID.vcf.gz | bgzip -c > merged.trimmed.$SGE_TASK_ID.vcf.gz
tabix merged.trimmed.$SGE_TASK_ID.vcf.gz
