#!/bin/bash
#
# Environment variables for processing WGS files
#

BASE_DIR=/scratch/u034/shared/wp5-gwas
PARAMS_DIR=$BASE_DIR/analysis/params
RESOURCES_DIR=$BASE_DIR/analysis/resources
REFERENCE_GENOME=$RESOURCES_DIR/hg38.fa
SCRIPTS_DIR=$BASE_DIR/analysis/wp5-gwas/scripts/wgs

export PATH=$PATH:/home/u034/shared/bin:/home/u034/shared/bin/gatk-4.1.8.1
