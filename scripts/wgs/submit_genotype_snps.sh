#!/bin/bash
#PBS -l walltime=48:00:00
#PBS -l ncpus=1,mem=8gb
#PBS -q sgp
#PBS -N genotype_snps
#PBS -j oe

# enable running singletons
if [ -z $PBS_ARRAY_INDEX ]
then
  if [ -z $INDEX ]
  then
    export PBS_ARRAY_INDX=1
  else
    export PBS_ARRAY_INDEX=$INDEX
  fi
fi

#
# Genotypes a set of GVCF files against a given set of SNP positions.
#
# Expects environment variables to be set:
#
# CONFIG_SH - absolute path to configuration script setting environment variables
#    * PARAMS_DIR
#    * PATH (to include bcftools, tabix, and gatk)
#    * REFERENCE_GENOME
# GVCF_FILES - path relative to PARAMS_DIR, list of GVCF files to process
# SNP_POSITIONS	- path relative to PARAMS_DIR, set of SNPs in interval format (https://gatk.broadinstitute.org/hc/en-us/articles/360035531852-Intervals-and-interval-lists)
# OUTPUT_DIR - absolute path to output directory
#

source $CONFIG_SH

echo PARAMS_DIR=$PARAMS_DIR
echo PATH=$PATH
echo REFERENCE_GENOME=$REFERENCE_GENOME
echo GVCF_FILES=$GVCF_FILES
echo SNP_POSITIONS=$SNP_POSITIONS
echo OUTPUT_DIR=$OUTPUT_DIR

GVCF_FILE=`head -n $PBS_ARRAY_INDEX $PARAMS_DIR/$GVCF_FILES | tail -n 1`
BNAME=`basename $GVCF_FILE`
VCF_FILE=${BNAME%gvcf.gz}vcf.gz
LOG_FILE=${BNAME%gvcf.gz}genotype.log
STATS_FILE=${BNAME%.gvcf.gz}.stats.txt

cd $OUTPUT_DIR
echo OUTPUT_FILE=$OUTPUT_DIR/$VCF_FILE
echo LOG_FILE=$OUTPUT_DIR/$LOG_FILE
echo STATS_FILE=$OUTPUT_DIR/$LOG_FILE

if [ `zcat $GVCF_FILE | head -n 10000 | grep FILTER | grep -c LowQual` == 0 ]
then

  echo 'Re-headering $GVCF_FILE to include ##FILTER=<ID=LowQual, Description="LowQual">'
  zcat $GVCF_FILE | head -n 10000 | grep ^## > ${BNAME%gvcf.gz}header.vcf
  echo '##FILTER=<ID=LowQual, Description="LowQual">' >> ${BNAME%gvcf.gz}header.vcf
  zcat $GVCF_FILE | head -n 10000 | grep ^#CHROM >> ${BNAME%gvcf.gz}header.vcf

  bcftools reheader -h ${BNAME%gvcf.gz}header.vcf $GVCF_FILE -o $BNAME
  tabix $BNAME

  GVCF_FILE=$BNAME

fi

gatk GenotypeGVCFs \
  -V $GVCF_FILE \
  -O $VCF_FILE \
  -R $REFERENCE_GENOME \
  --intervals $PARAMS_DIR/$SNP_POSITIONS \
  --force-output-intervals $PARAMS_DIR/$SNP_POSITIONS \
  --interval-padding 10 \
  &> $LOG_FILE

bcftools stats $VCF_FILE > $STATS_FILE

