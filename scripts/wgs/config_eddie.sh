#!/bin/bash
#
# Environment variables for processing WGS files
#

BASE_DIR=/exports/igmm/eddie/ISARIC4C/wp5-gwas
PARAMS_DIR=$BASE_DIR/analysis/params
RESOURCES_DIR=$BASE_DIR/analysis/resources
REFERENCE_GENOME=$RESOURCES_DIR/hg38.fa
SCRIPTS_DIR=$BASE_DIR/analysis/wp5-gwas/scripts/wgs

export PATH=$BASE_DIR/analysis/software/bin:$BASE_DIR/analysis/software/bin/gatk-4.1.8.1:$PATH
export BCFTOOLS_PLUGINS=/exports/igmm/eddie/ISARIC4C/wp5-gwas/analysis/software/install/bcftools-1.10.2/plugins
