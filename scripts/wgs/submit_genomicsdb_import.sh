#!/bin/bash
#PBS -l walltime=48:00:00
#PBS -l ncpus=64,mem=256gb
#PBS -q sgp
#PBS -N genomicsdb_import
#PBS -j oe

#
# Imports GVCF files into a GenomicsDB workspace for a given set of intervals.
# https://gatk.broadinstitute.org/hc/en-us/articles/360047216891-GenomicsDBImport
#
# If the workspace already exists, adds the samples to it.
#
# Expects environment variables to be set:
#
# CONFIG_SH - absolute path to configuration script setting environment variables
#    * PARAMS_DIR
#    * PATH (to include bcftools, tabix, and gatk)
#    * REFERENCE_GENOME
# SAMPLE_GVCF_MAP - path relative to PARAMS_DIR, tab-delimited text file mapping sample name to GVCF file, one per line
# SNP_POSITIONS	- path relative to PARAMS_DIR, set of SNPs in interval format (https://gatk.broadinstitute.org/hc/en-us/articles/360035531852-Intervals-and-interval-lists)
# OUTPUT_DIR - absolute path to output directory
#

source $CONFIG_SH

echo PARAMS_DIR=$PARAMS_DIR
echo PATH=$PATH
echo REFERENCE_GENOME=$REFERENCE_GENOME
echo SAMPLE_GVCF_MAP=$SAMPLE_GVCF_MAP
echo SNP_POSITIONS=$SNP_POSITIONS
echo OUTPUT_DIR=$OUTPUT_DIR

WORKSPACE=${SNP_POSITIONS%.bed}_genomicsdb_workspace
LOG_FILE=${SNP_POSITIONS%.bed}_${SAMPLE_GVCF_MAP%.txt}_genomicsdb_import.log

cd $OUTPUT_DIR
echo WORKSPACE=$OUTPUT_DIR/$WORKSPACE
echo LOG_FILE=$OUTPUT_DIR/$LOG_FILE

if [ -e $WORKSPACE ]
then

  # import to existing workspace - no need to specify intervals as the
  # ones for the existing workspace will be re-used

  gatk --java-options '-Xmx240g -Xmx240g' GenomicsDBImport \
    --genomicsdb-update-workspace-path $WORKSPACE \
    --batch-size 50 \
    --sample-name-map $PARAMS_DIR/$SAMPLE_GVCF_MAP \
    --reader-threads 63 \
  &> $LOG_FILE

else

  # import to new workspace

  gatk --java-options '-Xmx240g -Xms240g' GenomicsDBImport \
    --genomicsdb-workspace-path $WORKSPACE \
    --batch-size 50 \
    --intervals $PARAMS_DIR/$SNP_POSITIONS \
    --sample-name-map $PARAMS_DIR/$SAMPLE_GVCF_MAP \
    --reader-threads 63 \
  &> $LOG_FILE

fi
