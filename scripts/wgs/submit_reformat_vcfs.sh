#!/bin/bash
#PBS -l walltime=48:00:00
#PBS -l ncpus=1,mem=8gb
#PBS -q sgp
#PBS -N reformat_vcfs
#PBS -j oe

# enable running singletons
if [ -z $PBS_ARRAY_INDEX ]
then
  if [ -z $INDEX ]
  then
    export PBS_ARRAY_INDX=1
  else
    export PBS_ARRAY_INDEX=$INDEX
  fi
fi

#
# Genotypes a set of GVCF files against a given set of SNP positions.
#
# Expects environment variables to be set:
#
# CONFIG_SH - absolute path to configuration script setting environment variables
#    * PARAMS_DIR
#    * SCRIPTS_DIR
#    * PATH (to include bcftools, tabix, and gatk)
#    * REFERENCE_GENOME
# GVCF_FILES - path relative to PARAMS_DIR, list of GVCF files to process
# SNP_POSITIONS - path relative to PARAMS_DIR, BED file of SNP positions, requires chr:pos:ref:alt .txt extension version as well
# OUTPUT_DIR - absolute path to output directory
#

source $CONFIG_SH

echo PARAMS_DIR=$PARAMS_DIR
echo PATH=$PATH
echo REFERENCE_GENOME=$REFERENCE_GENOME
echo GVCF_FILES=$GVCF_FILES
echo OUTPUT_DIR=$OUTPUT_DIR

GVCF_FILE=`head -n $PBS_ARRAY_INDEX $PARAMS_DIR/$GVCF_FILES | tail -n 1`
BNAME=`basename $GVCF_FILE`
VCF_FILE=${BNAME%.gvcf.gz}.vcf.gz

echo VCF_FILE=$VCF_FILE

cd $OUTPUT_DIR

bcftools view -R $PARAMS_DIR/$SNP_POSITIONS $VCF_FILE | bcftools annotate -x FORMAT | perl $SCRIPTS_DIR/add_alt_allele.pl $PARAMS_DIR/${SNP_POSITIONS%.bed}.txt | bgzip -c  > ${VCF_FILE%.vcf.gz}.formatted.vcf.gz
tabix ${VCF_FILE%.vcf.gz}.formatted.vcf.gz
