#!/bin/bash
#PBS -l walltime=48:00:00
#PBS -l ncpus=1,mem=8gb
#PBS -q sgp
#PBS -N reheader_gvcfs
#PBS -j oe

# enable running singletons
if [ -z $PBS_ARRAY_INDEX ]
then
  if [ -z $INDEX ]
  then
    export PBS_ARRAY_INDX=1
  else
    export PBS_ARRAY_INDEX=$INDEX
  fi
fi

#
# Genotypes a set of GVCF files against a given set of SNP positions.
#
# Expects environment variables to be set:
#
# CONFIG_SH - absolute path to configuration script setting environment variables
#    * PARAMS_DIR
#    * PATH (to include bcftools, tabix, and gatk)
#    * REFERENCE_GENOME
# GVCF_FILES - path relative to PARAMS_DIR, list of GVCF files to process
# OUTPUT_DIR - absolute path to output directory
#

source $CONFIG_SH

echo PARAMS_DIR=$PARAMS_DIR
echo PATH=$PATH
echo REFERENCE_GENOME=$REFERENCE_GENOME
echo GVCF_FILES=$GVCF_FILES
echo OUTPUT_DIR=$OUTPUT_DIR

GVCF_FILE=`head -n $PBS_ARRAY_INDEX $PARAMS_DIR/$GVCF_FILES | tail -n 1`
BNAME=`basename $GVCF_FILE`

echo GVCF_FILE=$GVCF_FILE

cd $OUTPUT_DIR

if [ `zcat $GVCF_FILE | head -n 10000 | grep FILTER | grep -c LowQual` == 0 ]
then

  echo "Reheadering"

  zcat $GVCF_FILE | head -n 10000 | grep ^## > ${BNAME%gvcf.gz}header.vcf
  echo '##FILTER=<ID=LowQual, Description="LowQual">' >> ${BNAME%gvcf.gz}header.vcf
  zcat $GVCF_FILE | head -n 10000 | grep ^#CHROM >> ${BNAME%gvcf.gz}header.vcf

  bcftools reheader -h ${BNAME%gvcf.gz}header.vcf $GVCF_FILE | \
    bcftools annotate -x FORMAT/NML |
    bgzip -c > $BNAME
  tabix $BNAME

else

  echo "Header is correct - creating symbolic links"

  ln -s $GVCF_FILE $BNAME
  ln -s $GVCF_FILE.tbi $BNAME.tbi

fi
