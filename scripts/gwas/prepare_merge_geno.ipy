import pandas as pd

###  ###

plink = "./bin/plink"

bim_cols = ['chrom','rsid','cm','bp','A1','A2']

### Extract subsets of common SNPs from all datasets ###

def load_snps(ds):
    snps = {}
    # load snps for each dataset and:
    # - add common id based on chr,bp
    # - remove multi-allele snps (= multiple snps with same chr,bp)
    for i in ds:
        df = pd.read_csv(ds[i]+'.bim',sep='\t',names=bim_cols)  
        df['pos_id'] = df.chrom.astype(str) + '_' + df.bp.astype(str)
        n = df.groupby('pos_id').size()
        df = df[df.pos_id.isin(n[n==1].index)]
        snps[i] = df.set_index('pos_id').copy()

    return pd.concat(snps,axis=1).dropna()

def flip_strand(x):
    subs = {'A':'T','T':'A','C':'G','G':'C'}
    return ''.join(map(lambda l: subs[l], list(x)))

def extract_common_snps(ds, snps):
    i,j = ds.keys()
    
    # find common SNPs, allowing for allele and strand swaps
    match      = (snps[i].A1 == snps[j].A1) & (snps[i].A2 == snps[j].A2)
    match_swap = (snps[i].A1 == snps[j].A2) & (snps[i].A2 == snps[j].A1)
    match_flip = (snps[i].A1.map(flip_strand) == snps[j].A1) & (snps[i].A2.map(flip_strand) == snps[j].A2)
    match_flip_swap = (snps[i].A1.map(flip_strand) == snps[j].A2) & (snps[i].A2.map(flip_strand) == snps[j].A1)

    common = match | match_swap | match_flip | match_flip_swap
    
    # find A/T and C/G snps and exclude them, as strand ambigious
    at_or_cg = False
    for allele in [('A','T'),('C','G')]:
        at_or_cg |= ((snps[i].A1 == allele[0]) & (snps[i].A2 == allele[1])) | ((snps[i].A1 == allele[1]) & (snps[i].A2 == allele[0]))

    # save information for plink
    snps[i].rsid[common & ~at_or_cg].to_csv(f"./tmp/{i}.common.rsid",header=False,index=False)
    snps[j].rsid[common & ~at_or_cg].to_csv(f"./tmp/{j}.common.rsid",header=False,index=False)
    snps[i].rsid[(match_flip | match_flip_swap) & ~at_or_cg].to_csv(f"./tmp/{i}.flip.rsid",header=False,index=False)

    print(f"Common={common.mean()}, A/T,C/G = {at_or_cg.mean()}, Used={(common & ~at_or_cg).mean()}")
    
    #extract and flip strand in one
    !{plink} --bfile {ds[i]} --extract ./tmp/{i}.common.rsid --flip ./tmp/{i}.flip.rsid --make-bed --out ./tmp/{i}.common > /dev/null
    !{plink} --bfile {ds[j]} --extract ./tmp/{j}.common.rsid --make-bed --out ./tmp/{j}.common > /dev/null

    #rename snps to common scheme 
    for i in ds:
        df = pd.read_csv(f"./tmp/{i}.common.bim",sep='\t',names=bim_cols)
        df['rsid'] = df.chrom.astype(str) + '_' + df.bp.astype(str)
        df.to_csv(f"./tmp/{i}.common.renamed.bim",sep='\t',index=False,header=False)

def merge_common(ds, fnOut):
    extract_common_snps(ds, load_snps(ds))
    i,j = ds.keys()
    !{plink} --bed ./tmp/{i}.common.bed --bim ./tmp/{i}.common.renamed.bim --fam ./tmp/{i}.common.fam --bmerge ./tmp/{j}.common.bed ./tmp/{j}.common.renamed.bim ./tmp/{j}.common.fam --make-bed --out {fnOut}
    
    #map FIDs,IIDs to standard format (lower case and set FID to IID)  
    !cp {fnOut}.fam {fnOut}.fam.orginal 
    fam = pd.read_csv(f"{fnOut}.fam",delim_whitespace=True,header=None,dtype=str)
    fam[1] = fam[1].map(lambda x: x.lower())
    fam[0] = fam[1]
    fam.to_csv(f"{fnOut}.fam",sep='\t',header=False,index=False)
    
def main():
    fnGenoGen = './data/geno.raw/r4_plink_20200824/wp5-gwas-r4-as'
    dirOut    = './data/geno/r4/'
    !mkdir -p {dirOut}
    what = [#(f'{dirOut}/merged.cal',{'ukb':'./data/geno.raw/ukb.cal', 'gen':fnGenoGen}),
            (f'{dirOut}/merged.imp',{'ukb':'./data/geno.raw/ukb.imp', 'gen':fnGenoGen})
           ]
    for fnOut,ds in what:
        print(f"Creating {fnOut}")
        merge_common(ds,fnOut)
        
if __name__ == '__main__':
    main()