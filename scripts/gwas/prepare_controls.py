import pandas as pd
import numpy as np

def matched(cases, controls, rel):
    cases = cases.copy()
    candidates = controls.copy()
    cases['Matched_Control_IID'] = np.nan
    for i in cases.index:
        case = cases.loc[i]
        no_match =  pd.Series(False,index=candidates.index)
        ### Match criteria ###
        match_ethn = candidates.POP == case.POP if case.POP != '-' and not '+' in case.POP else no_match
        match_age  = candidates.Age == case.Age if case.Age >= 0 else no_match
        match_sex  = candidates.Sex == case.Sex if case.Sex != 'unknown' else no_match
        #match_pc  = candidates.Postcode == case.Postcode if case.Postcode != 'unknown' else no_match
        match_dd  = controls.Deprivation_Decile == float(case.Deprivation_Decile) if case.Deprivation_Decile != 'unknown' else no_match 
        matches = candidates[match_age & match_sex & match_dd & match_ethn].copy()
        
        if matches.shape[0] > 0:
            match = None
            #Sample match and keep first who is unrelated to other controls
            while match is None and matches.shape[0] > 0:
                match = matches.sample().index[0]
                j = (match == rel[['ID1','ID2']]).any(axis=1)
                if (cases.Matched_Control_IID.isin(rel[j].ID1) | cases.Matched_Control_IID.isin(rel[j].ID2)).any():
                    matches.drop(match,inplace=True)
                    match = None
            if match is not None:
                cases.loc[i,'Matched_Control_IID'] = match
                candidates.drop(match,inplace=True)
    return cases

def random(cases, controls, rel, num_controls = 5):
    cases = cases.copy()
    
    a = rel[['ID1','ID2']]
    b = rel[['ID2','ID1']]
    b.columns = ['ID1','ID2']
    pairs = pd.concat([a,b])
    
    candidates = controls.copy()
    blocked = pd.DataFrame(columns=['ID1','ID2'])
    for i in cases.index:
        case = cases.loc[i]
        no_match =  pd.Series(False,index=candidates.index)
        ### Match criteria ###
        match_ethn = candidates.POP == case.POP if case.POP != '-' and not '+' in case.POP else no_match
        matches = candidates[match_ethn]
        
        for k in range(num_controls):
            match = None
            while match is None and matches.shape[0] > 0:
                match = matches.sample().index[0]
                j = (match == rel[['ID1','ID2']]).any(axis=1)
                if match in blocked.ID2:
                    matches.drop(match,inplace=True)
                    match = None
            if match is not None:
                cases.loc[i,f'Rand_Control_{k}_IID'] = match
                candidates.drop(match,inplace=True)
                matches.drop(match,inplace=True)
                blocked = pd.concat([blocked, pairs[pairs.ID1 == match]])
    return cases

def main():
    fnData = "./data/prepared/r4/{0}.data.csv"
    fnRel  = "./data/ukb.input/ukb493_rel_s488374.dat"
    fnOut  = "./data/controls.r4.csv"
    
    data = {i:pd.read_csv(fnData.format(i),index_col=0) for i in ['ukb','genomicc']}
    covars = ['Sex','Age','Deprivation_Decile','POP'] + [f'PC{i}' for i in range(1,11)]
    #change to exclude all UKB who had a test even if negative
    controls = data['ukb'].loc[(data['ukb'].Covid19_Test == 'NoTest'), covars].dropna()
    cases    = data['genomicc']

    rel = pd.read_csv(fnRel,delim_whitespace=True)
    rel = rel[~(rel[['ID1','ID2']] == -1).any(axis=1)]
    rel = rel[rel.Kinship > 0.0442]

    cases = matched(cases, controls, rel)
    cases = random(cases, controls, rel)
    
    cases[['Matched_Control_IID'] + [f'Rand_Control_{k}_IID' for k in range(5)]].applymap(lambda x: str(int(x)) if not np.isnan(x) else "").to_csv(fnOut)
    
if __name__ == '__main__':
    main()