for pheno in broad narrow
do
    for controls in rand matched
    do
        echo ${pheno} ${controls}
        dirWork=./analysis/gwas_${pheno}_${controls}/
        Rscript step1_fitNULLGLMM.R     \
                --plinkFile=${dirWork}geno.grm \
                --phenoFile=${dirWork}data.tsv \
                --phenoCol=Case \
                --covarColList=Age,AgeXAge,Sex,Deprivation_Decile,PC1,PC2,PC3,PC4,PC5,PC6,PC7,PC8,PC9 \
                --sampleIDColinphenoFile=IID \
                --traitType=binary        \
                --outputPrefix=${dirWork}saige.step1.output \
                --nThreads=4 \
                --LOCO=TRUE \
                --IsOverwriteVarianceRatioFile=TRUE > ${dirWork}saige.step1.log

        for chrom in $(seq 1 22)
        do
            echo "chr"${chrom}
            Rscript step2_SPAtests.R	\
                    --vcfFile=${dirWork}geno.vcf.gz \
                    --vcfField=GT \
                    --vcfFileIndex=${dirWork}geno.vcf.gz.tbi \
                    --chrom=${chrom} \
                    --minMAF=0.0001 \
                    --minMAC=1 \
                    --GMMATmodelFile=${dirWork}saige.step1.output.rda \
                    --varianceRatioFile=${dirWork}saige.step1.output.varianceRatio.txt \
                    --SAIGEOutputFile=${dirWork}saige.step2.output.chrom${chrom} \
                    --numLinesOutput=2 \
                    --IsOutputAFinCaseCtrl=TRUE > ${dirWork}saige.step2.chrom${chrom}.log
        done
    done
done